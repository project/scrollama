<?php

namespace Drupal\Tests\scrollama\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Functional tests for the Scrollama Drupal module.
 */
class ScrollamaLibraryIsAvailableTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['scrollama'];


  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the Scrollama library is loaded when automatic loading is enlabed.
   */
  public function testScrollamaLibraryIsLoadedWhenEnabled() {

    $this->config('scrollama.settings')
      ->set('enable_globally', TRUE)
      ->save();

    $this->drupalGet('<front>');

    $scrollamaExists = $this->getSession()->evaluateScript('return typeof scrollama !== "undefined";');
    $this->assertTrue($scrollamaExists, 'The scrollama variable exists in the global JavaScript scope.');
  }

}
