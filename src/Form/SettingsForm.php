<?php

namespace Drupal\scrollama\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure scrollama settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scrollama_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['scrollama.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['enable'] = [
      '#type' => 'fieldset',
      '#title' => 'Enable scrollama',
      '#description' => $this->t('Use this options to load scrollama libraries on all pages. This come with a performance penalty if the library is not in use across the whole site. Otherwise, we recommend to conditionally attach "scrollama/scrollama" and "scrollama/scrollama-css" libraries on your code only when needed, using one of the many ways available in Drupal'),
    ];
    $form['enable']['enable_globally'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable scrollama globally'),
      '#description' => $this->t("Consider adding 'scrollama/scrollama' library as a dependency on your code for conditional loading."),
      '#default_value' => $this->config('scrollama.settings')->get('enable_globally'),
    ];
    $form['enable']['enable_css'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable CSS animations'),
      '#description' => $this->t("Consider adding 'scrollama/scrollama-css' library as a dependency on your code for conditional loading."),
      '#default_value' => $this->config('scrollama.settings')->get('enable_css'),
    ];

    $form['config'] = [
      '#type' => 'fieldset',
      '#title' => 'Configuration',
    ];
    $form['config']['offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Offset'),
      '#description' => $this->t("A number from 0 to 1, where 0 is the viewport top and 1 the bottom."),
      '#default_value' => $this->config('scrollama.settings')->get('offset'),
      '#min' => 0,
      '#max' => 1,
      '#step' => 0.01,
    ];
    $form['config']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#description' => $this->t("Show Scrollama's debug mode and print to console useful data from animated elements."),
      '#default_value' => $this->config('scrollama.settings')->get('debug'),
    ];
    $form['config']['order'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fire previous step triggers'),
      '#description' => $this->t("If the user lands on an already scrolled page, this option will ensure all previous elements are triggered on page load."),
      '#default_value' => $this->config('scrollama.settings')->get('order'),
    ];
    $form['config']['once'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fire only once per item'),
      '#description' => $this->t("Only trigger the step to enter once then remove listener."),
      '#default_value' => $this->config('scrollama.settings')->get('once'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('scrollama.settings')
      ->set('enable_globally', $form_state->getValue('enable_globally'))
      ->set('debug', $form_state->getValue('debug'))
      ->set('enable_css', $form_state->getValue('enable_css'))
      ->set('offset', $form_state->getValue('offset'))
      ->set('order', $form_state->getValue('order'))
      ->set('once', $form_state->getValue('once'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
