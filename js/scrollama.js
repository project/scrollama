(function (Drupal, debounce, once) {
  Drupal.scrollama = {};

  Drupal.scrollama.getScrollamaItemsData = function (elements) {
    const elementsData = [];

    function validTimeUnit(string) {
      const match = string.match(/^((?:0.)?\d+)s?$/);

      if (match) {
        return match[1];
      }
      return false;
    }

    elements.forEach((element) => {
      const init = element.getAttribute('data-scroll-init');
      const exit = element.getAttribute('data-scroll-exit');
      const delay = element.getAttribute('data-scroll-delay');

      if (!init) {
        throw new Error(
          'You must provide at least a value for "data-scroll-init"',
        );
      }
      const data = {
        element,
        init: init.trim().split(/\s+/),
      };

      if (exit) {
        data.exit = exit.trim().split(/\s+/);
      }

      if (delay) {
        // Delete the "s" unit and detect wrong options.
        const filteredDelay = validTimeUnit(delay);

        if (filteredDelay) {
          data.delay = parseInt(filteredDelay, 10);
        } else {
          throw new Error(
            'Please express your data-scroll-delay in seconds (i.e: "0.75" or "0.75s")',
          );
        }
      } else {
        data.delay = 0;
      }

      elementsData.push(data);
    });

    return elementsData.length > 0 ? elementsData : false;
  };

  Drupal.behaviors.scrollama = {
    attach(context, settings) {
      const elements = once('scrollama', '[data-scroll-init]', context);

      // Quick exit if there are no scroll-enabled elements.
      if (elements.length <= 0) {
        return;
      }

      // Get all the element's data in order to cache it in a variable
      // and improve performance.
      const elementsData = Drupal.scrollama.getScrollamaItemsData(elements);

      if (settings.scrollama.debug) {
        console.table(
          elementsData.map(function (item) {
            return {
              item: item.element,
              init: item.init.join(' '),
              exit: item.exit ? item.exit.join(' ') : false,
              delay: item.delay ? `${item.delay}s` : false,
            };
          }),
        );
      }

      const scroller = scrollama();
      scroller
        .setup({
          step: elements,
          offset: settings.scrollama.offset,
          debug: settings.scrollama.debug,
          once: settings.scrollama.once,
          order: settings.scrollama.order,
        })
        .onStepEnter(function (response) {
          const elementData = elementsData[response.index];
          setTimeout(function () {
            elementData.element.classList.add(elementData.init);
          }, elementData.delay * 1000);
        })
        .onStepExit(function (response) {
          const elementData = elementsData[response.index];

          if (elementData.exit) {
            setTimeout(function () {
              elementData.element.classList.add(elementData.exit);
            }, elementData.delay * 1000);
          }
        });

      window.addEventListener('resize', debounce(scroller.resize, 500));
    },
  };
})(Drupal, Drupal.debounce, once);
