# Scrollama for Drupal8/9

Loads [scrollama.js](https://github.com/russellgoldenberg/scrollama) library and provides a simple `data-attributes`-based API for triggering changes (animations, etc) via css classes.

## Installation

- Install and enable the module.
- Activate the module by one of these two ways:
  - Go to the module configuration page and enable it globally. This will load scrollama on all pages, so it could come with a performance penalty if the library is not in use across your site. It is useful for testing, but for production we recommend switching to the second option.
  - Attach the `"scrollama/scrollama"` library on your code when needed, using [one of the many ways available in Drupal](https://www.drupal.org/docs/creating-custom-modules/adding-stylesheets-css-and-javascript-js-to-a-drupal-module)

**Please note** that, for performance reasons, scrollama is **disabled** by default. It's up to you to choose one of the previous two ways of activation.

## How to use

On its core, Scrollama is a library that will only set a scroll point on the viewport and react whenever a given HTML element enters and exits this point. [See a working example](https://russellsamora.github.io/scrollama/basic/).

This means **is up to you to decide what to do**. Scrollama won't animate, enable, disable, reposition or change your HTML. You will.

This module adds a simple data-attributes-based API as a helper to improve and fasten development.

- Add `data-scroll-init="my-class"` to any HTML element to add `.my-class` when entering the screen. You can add multiple classes by separating them by at least a space, e.g. `data-scroll-init="my-class another-class"`.
- Optionally, add `data-scroll-exit="my-exit-class"` to trigger a class on exit and `data-scroll-delay="1"` to add a delay (expressed in seconds).

### Example:

```html
<div data-scroll-init="fade-in" data-scroll-exit="fade-out" data-scroll-delay="2">
  My code...
</div>
```

This element will fade in when entering scroll point, fade out when exiting and  the animation will have a delay of 2 seconds.

### CSS animations:

This module ships with a very simple CSS implementation of several common animations. You can use the CSS as it is or as a reference for your implementation.

- `.fade-in` will fade the element from opacity 0 to 1.
- `.fade-out` will do the opposite
- `.slide-in` will move the element on the Y-axis from -200px to 0.
- `.slide-out` will move the element on the Y-axis from 200px to 0.

To enable it, attach the library `"scrollama/scrollama-css"` to your code. TODO: allow the user to enable it via admin UI.


## TODOs and frequently asked questions

### Where is the scroll point located?

The scroll point is hardcoded at 75% of the screen starting from the top. You can enable debug mode to see it. TODO: allow the user to define this.

### How can I debug?

Go to the configuration page and enable debug mode. You will see the scroll point and the console will log all the data-enabled elements and its properties.

### Do the triggers re-launch themselves if the user scrolls up and down?

By default, the triggers only run once. This means that the user will see your effect only once per scroll. TODO: allow the user to define this.

### If a user lands in the middle of a screen, will the previous triggers be activated?

Yes, if the user arrives to the page scrolled at point X, all the triggers before X will be immediately triggered. TODO: allow the user to define this.
